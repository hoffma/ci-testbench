# My personal CI-testbench

This repository is just a simple workbench to try my way through Gitlab-CI and
hopefully not putting to much trash into other project repositories.

# Build and register custom Docker image


## Create .gilab-ci.yml

See [.gitlab-ci.yml](.gitlab-ci.yml) as example 

## Create Dockerfile

Note to myself: Gitlab-CI does not need the extra Dockerfile. Everything is
handled by the gitlab-ci.yml. The Dockerfile is neat to run the project locally
in a container

See [Dockerfile](Dockerfile)


## Register the Dockerfile under registries

Note to myself: This is probably only needed for more complex tasks.\
Just use existing Docker-Images and try to solve the problem with them.

```bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/hoffma/ci-testbench .
docker push registry.gitlab.com/hoffma/ci-testbench
```


I think by default gitlab should recognize all this and run it automatically?
