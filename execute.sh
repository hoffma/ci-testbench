#!/usr/bin/env bash

if [[ $BUILD_PATH == "" ]]; then
  echo "Error: BUILD_PATH is empty." 1>&2
  exit 1
else
  echo "BUILD_PATH is: ${BUILD_PATH}"
fi

# Random bash script which finds a random number where the digit sum is divisible by 3

SCRIPT_PATH="$(dirname $0)/$(basename $0)"
echo "Executing script: ${SCRIPT_PATH}"

RES=1

while ((RES != 0)); do
  NUM=$RANDOM
  DIG_SUM=0
  echo "Got random number: ${NUM}"

  while ((NUM != 0)); do
    TMP=$((NUM%10))
    DIG_SUM=$((DIG_SUM + TMP))
    NUM=$((NUM/10))
  done

  echo "Digit sum is ${DIG_SUM}"
  RES=$((DIG_SUM%3))
done

exit $RES
