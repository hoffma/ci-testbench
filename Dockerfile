# just the linux image to use
FROM ubuntu:22.04

ADD execute.sh /
ADD build.sh /
ADD check_executables.sh /
ADD test_hello.sh /
ADD hello /usr/local/bin

RUN apt-get update
RUN apt-get install -y bash

CMD ["echo", "Hello custom docker"]
RUN chmod +x /usr/local/bin/hello
RUN bash build.sh
RUN bash check_executables.sh
RUN bash test_hello.sh
