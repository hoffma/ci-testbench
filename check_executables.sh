#!/usr/bin/env bash

arr=(hello)

# Just elements.
for element in "${arr[@]}"; do
    elem_path=$(which $element)
    if [ -x "$elem_path" ]; then
      echo "${element} found at: ${elem_path}"
    else
      printf 'Executable for "%s" not found\n' "$element"
      exit 1
    fi
done


exit 0

